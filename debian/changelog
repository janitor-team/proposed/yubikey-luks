yubikey-luks (0.5.1+29.g5df2b95-6) unstable; urgency=medium

  * [4a7b14f] patch: Stop exposing challenge in process list (Closes: #984709)

 -- Markus Frosch <lazyfrosch@debian.org>  Mon, 08 Mar 2021 20:27:04 +0100

yubikey-luks (0.5.1+29.g5df2b95-5) unstable; urgency=medium

  * [82ebd58] d/control: Set maintainer address to authentication team
    (Closes: #979065)

 -- Markus Frosch <lazyfrosch@debian.org>  Sat, 02 Jan 2021 17:39:12 +0100

yubikey-luks (0.5.1+29.g5df2b95-4) unstable; urgency=medium

  [ Markus Frosch ]
  * [6fd9f9d] Switch to salsa-ci config

  [ Debian Janitor ]
  * [bb3a508] Bump debhelper from old 11 to 12.
  * [5a74026] Set debhelper-compat version in Build-Depends.
  * [0fb85b0] Set upstream metadata fields: Bug-Database, Bug-Submit.

  [ Markus Frosch ]
  * [93a1024] Bump Standards Version to 4.5.1 and debhelper to 13

 -- Markus Frosch <lazyfrosch@debian.org>  Sat, 02 Jan 2021 13:21:45 +0100

yubikey-luks (0.5.1+29.g5df2b95-3) unstable; urgency=medium

  * Rebuild for source-only upload

 -- Markus Frosch <lazyfrosch@debian.org>  Sun, 29 Dec 2019 16:48:19 +0100

yubikey-luks (0.5.1+29.g5df2b95-2) unstable; urgency=medium

  [ Kaylie VonRueden ]
  * [8129447] yubikey-luks-open should be in $PATH

  [ Markus Frosch ]
  * [a06de1a] Add gitlab-ci config
  * [1f099da] Recommend expect for yubikey-luks-open (Closes: #946370)
  * [ff2d306] Only depend on cryptsetup-run and recommend cryptsetup-initramfs
    (Closes: #916314)

 -- Markus Frosch <lazyfrosch@debian.org>  Sat, 28 Dec 2019 20:50:53 +0100

yubikey-luks (0.5.1+29.g5df2b95-1) unstable; urgency=medium

  [ Nicolas Braud-Santoni ]
  * [8f36458] Replace dh_clean step with a patch
  * New upstream version 0.5.1 (2017-11-26) (Closes: #912442)
  * Add myself to uploaders
  * Install files for suspend support
    The integration scripts from upstream only support systemd; this should
    be fixed ASAP, and I will open a bug once this version hit the archive.

  * Comply with policy 4.1.4
    + Replace Priority: extra with optional
    + Switch to debhelper 11
  * Replace custom debian/rules with debian/install
  * Move packaging repository to salsa.d.o
  * Remove obsolete, Debian manpage (upstream ships an up-to-date one)

  [ Markus Frosch ]
  * [af09266] Detect path of crypttab in initramfs [patch]
  * [5ea2ae2] Disable yubikey-luks-suspend.service by default
  * [53c8f3d] Add warning about yubikey-luks-suspend.service
  * [76850f2] New upstream version 0.5.1+29.g5df2b95
    (Closes: #911734) (Closes: #904162)
  * [ebf80df] Remove obsolete patches
  * [9577dc0] initramfs: No longer install top/bottom scripts
  * [301ce09] Add NEWS about required keyscript

 -- Markus Frosch <lazyfrosch@debian.org>  Wed, 23 Jan 2019 13:48:51 +0100

yubikey-luks (0.3.3+3.ge11e4c1-1) unstable; urgency=medium

  * Initial release. (Closes: #795982)

 -- Markus Frosch <lazyfrosch@debian.org>  Mon, 17 Aug 2015 11:47:27 +0200
